/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	//use this logger for test
	"bitbucket.org/n0needt0/uppr"
	"errors"
	"fmt"
	//use this for production
	//logging "github.com/op/go-logging"
	"runtime"
	"strings"
	"time"
)

/*

USE THIS LOGGER FOR PRODUCTION
var log *logging.Logger

func SetLog(NewLog *logging.Logger) {
	log = NewLog
}
*/

//for test only
var log = uppr.GetLog()

var (
	STATE_UP          = "PASS"
	STATE_DOWN        = "FAIL"
	TESTTIMEOUTDEFSEC = 20 //default
	TESTTIMEOUTMAXSEC = 90
	TRACEROUTETIMEOUT = 45
	ENV_PINGCMD       = "ENV_PINGCMD"
	ENV_CURLCMD       = "ENV_CURLCMD"
)

//this is basic job type
//it can be defined, run and receive results
//Retrace will enable traceroute for job
//runtrace if for internal use
type Job struct {
	Chkid    string                 `json:"chkid", bson:"chkid"`
	Chktype  string                 `json:"chktype", bson:"chktype"`
	Config   map[string]interface{} `json:"config", bson:"config"`
	Retrace  bool                   `json:"retrace", bson:"retrace"`
	Runtrace bool                   `json:"runtrace,omitempy", bson:"runtrace,omitempty"`
	Result   map[string]string      `json:"result,omitempy", bson:"result,omitempty"`
}

func (j Job) setcfgdefault(key string, value interface{}) Job {
	j.Config[key] = value
	j.Config[fmt.Sprintf("setdefault*%s", key)] = value
	return j
}

func (j Job) setcfgvalue(key string, value interface{}) Job {
	j.Config[key] = value
	return j
}

//Test type is genericfunction signature of all test functions.
//it receives job and two channels
type Test func(Job, chan Job)

//if traceroutes are possible on file
func TraceOnFail(chktype string) bool {
	switch chktype {
	case "GET", "HEAD", "POST", "TCP", "PING", "DNS", "FTP", "LDAP", "SQL", "SSH", "SMTP":
		return true
	default:
		return false
	}
}

//maps check timpe tu func
func TestMap(test string) (Test, error) {

	var runner Test

	switch strings.ToUpper(test) {
	case "DNS":
		runner = f_dns()
	case "HTTP_GET", "HTTP_HEAD", "HTTP_POST", "HTTP":
		runner = f_http()
	case "TCP":
		runner = f_tcp()
	case "PING":
		runner = f_ping()
	case "TRACE":
		runner = f_trace()
	case "FTP":
		runner = f_ftp()
	case "LDAP":
		runner = f_ldap()
	case "SQL":
		runner = f_sql()
	case "SSH":
		runner = f_ssh()
	case "SMTP":
		runner = f_smtp()
	default:
		return runner, errors.New(fmt.Sprintf("Invalid test type %s", test))
	}
	return runner, nil
}

/*And this block will run the actual test from implementing struct*/
func RunTest(job Job, inch chan Job, outch chan Job) {

	//this is where runner function will output its data
	testch := make(chan Job)

	//runner function retrieved from job config
	runner, err := TestMap(job.Chktype)
	if err != nil {
		log.Error(err.Error())
		//give it back
		outch <- job
		return
	}

	//we got out here to run trace again
RETRACE:

	if job.Runtrace == true {
		log.Debug("Set Locks")
		//we replace runner with retrace runner
		runner = f_retrace()
		job.Runtrace = false
		job.Retrace = false
		//give retrace bit time
		job.Config["timeout"] = TESTTIMEOUTDEFSEC
	}

	k := keyr.New(job.Config)

	timeout := GetTimeout(k, job)

	log.Debug(fmt.Sprintf("JOB starting %+v", job))
	//fire runner function in separate routime
	//if this routine hangs until next statement will
	//timeout or returned
	//runner = runner
	go runner(job, testch)

	//wait for result or timeout
	for {
		select {
		//if failed send to trace
		case testjob := <-testch:
			log.Debug(fmt.Sprintf("JOB complete %+v", testjob))
			if testjob.Retrace {
				if state, ok := testjob.Result["state"]; ok && state == STATE_DOWN {
					testjob.Runtrace = true
					testjob.Retrace = false
					log.Info(fmt.Sprintf("Retracing on fail %+v", testjob))
					//now we re-run this job,
					job = testjob
					goto RETRACE
				}
			}
			outch <- testjob
			close(testch)
			return
			//we pad timeout by 2 sec so test has time to complete
		case <-time.After(time.Duration(timeout+2) * time.Second):
			//it failed so send to trace
			job.Result["msg"] = fmt.Sprintf("Total test duration has exceeded %d seconds", timeout)
			job.Result["total_ms"] = fmt.Sprintf("%d", timeout*1000)
			if job.Retrace {
				job.Runtrace = true
				job.Retrace = false
				log.Debug(fmt.Sprintf("Retracing on timeout %+v", job))
				//now we re-run this job,
				goto RETRACE
			}
			outch <- job
			log.Debug(fmt.Sprintf("Closing testchan timeout:%d, %+v", timeout, job))
			close(testch)
			return
		}
		runtime.Gosched()
	}
	return
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func timeIt(start int64) string {
	return fmt.Sprintf("%d", makeTimestamp()-start)
}

func GetTimeout(k keyr.Keyr, job Job) int {

	v, err := k.GetKeyAsInt("timeout")
	if err != nil {
		return TESTTIMEOUTDEFSEC
	}

	if v < 1 || v > TESTTIMEOUTMAXSEC {
		return TESTTIMEOUTDEFSEC
	}
	return v
}

func GetPingPacketTimeoutMsec(k keyr.Keyr, job Job) int {

	packet_timeout_msec, err := k.GetKeyAsInt("packet_timeout_msec")
	if err != nil || packet_timeout_msec < 100 || packet_timeout_msec > 10000 { //1 second default
		packet_timeout_msec = 1000
	}

	return packet_timeout_msec
}
