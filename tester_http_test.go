/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	colors "bitbucket.org/n0needt0/libs"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHTTP(t *testing.T) {

	log.Debug(colors.Cyan("TestHTTP"))

	var testdata map[string]map[string]interface{}

	//can not use head with anything but code
	//"head_by_cookie_ok":{"PASS":"PASS","request": "head","uri": "http://httpbin.org/get", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest1"]},
	//"head_by_cookie_fail":{"PASS":"FAIL","request": "head","uri": "http://httpbin.org/get", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest3"]}

	/*
		"get_by_cookie_ok":{"PASS":"PASS","request": "get","uri": "http://httpbin.org/get", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest1"]},
		"get_by_cookie_fail":{"PASS":"FAIL","request": "get","uri": "http://httpbin.org/get", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest3"]},
		"post_by_cookie_ok":{"PASS":"PASS","request": "post","uri": "http://httpbin.org/post", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest1"]},
		"post_by_cookie_fail":{"PASS":"FAIL","request": "post","uri": "http://httpbin.org/post", "cookies":["fake1=faketest1", "fake2=faketest2"], "ok_strings": ["faketest3"]},
		"post_by_header_fail":{"PASS":"FAIL","request": "post","uri": "http://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "headers":["Content-Fake:faketest"], "ok_strings": ["faketest1"]},
		"get_by_code_fail":  {"PASS": "FAIL", "request": "get", "uri": "http://httpbin.org/get","ok_codes": ["2001"]},
		"get_by_string_ok":  {"PASS":"PASS", "request": "get","uri": "https://httpbin.org/get?musikpusi=1", "ok_strings": ["musikpusi"]},
		"get_by_string_fail":{"PASS":"FAIL","request": "get","uri": "http://httpbin.org/get?musikpusi=1", "ok_strings": ["musikpusi!"]},
		"get_by_header_ok":  {"PASS":"PASS", "request": "get","uri": "http://httpbin.org/get", "headers":["Content-Fake:faketest"], "ok_strings": ["faketest"]},
		"get_by_header_fail":{"PASS":"FAIL","request": "get","uri": "http://httpbin.org/get", "headers":["Content-Fake:faketest"], "ok_strings": ["faketest1"]},
		"get_by_code_ok_ssl": 	 {"PASS": "PASS", "request": "get", "uri": "https://httpbin.org/get","ok_codes": ["200"]},
		"get_by_code_fail_ssl":  {"PASS": "FAIL", "request": "get", "uri": "https://httpbin.org/get","ok_codes": ["2001"]},
		"get_by_string_ok_ssl":  {"PASS":"PASS", "request": "get","uri": "https://httpbin.org/get?musikpusi=1", "ok_strings": ["musikpusi"]},
		"get_by_string_fail_ssl":{"PASS":"FAIL","request": "get","uri": "https://httpbin.org/get?musikpusi=1", "ok_strings": ["musikpusi!"]},
		"get_by_header_ok_ssl":  {"PASS":"PASS", "request": "get","uri": "https://httpbin.org/get", "headers":["Content-Fake:faketest"], "ok_strings": ["faketest"]},
		"get_by_header_fail_ssl":{"PASS":"FAIL","request": "get","uri": "https://httpbin.org/get", "headers":["Content-Fake:faketest"], "ok_strings": ["faketest1"]},
		"post_by_code_ok_ssl": 	 {"PASS": "PASS", "request": "post", "uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_codes": ["200"]},
		"post_by_code_fail_ssl":  {"PASS": "FAIL", "request": "post", "uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_codes": ["2001"]},
		"post_by_string_ok_ssl":  {"PASS":"PASS", "request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_strings": ["mus1kpusik2"]},
		"post_by_string_fail_ssl":{"PASS":"FAIL","request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_strings": ["mus1kpusik3"]},
		"post_by_header_ok_ssl":  {"PASS":"PASS", "request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "headers":["Content-Fake:faketest"], "ok_strings": ["faketest"]},
		"post_by_header_ok":  {"PASS":"PASS", "request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "headers":["Content-Fake:faketest"], "ok_strings": ["faketest"]},
		"post_by_header_fail_ssl":{"PASS":"FAIL","request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "headers":["Content-Fake:faketest"], "ok_strings": ["faketest1"]},
		"head_by_code_ok":{"PASS":"PASS","request": "head","uri": "http://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest"], "ok_codes":["200"]},
		"head_by_code_fail":{"PASS":"FAIL","request": "head","uri": "http://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest1"], "ok_codes":["2001"]},
		"head_by_header_ok":{"PASS":"PASS","request": "head","uri": "http://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest"]},
		"head_by_header_fail":{"PASS":"FAIL","request": "head","uri": "http://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest1"]},
		"head_by_code_ok_ssl":{"PASS":"PASS","request": "head","uri": "https://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest"], "ok_codes":["200"]},
		"head_by_code_fail_ssl":{"PASS":"FAIL","request": "head","uri": "https://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest1"], "ok_codes":["2001"]},
		"head_by_header_ok_ssl":{"PASS":"PASS","request": "head","uri": "https://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest"]},
		"head_by_header_fail_ssl":{"PASS":"FAIL","request": "head","uri": "https://httpbin.org/response-headers?faketest=faketest", "headers":["Content-Fake:faketest"], "ok_headers": ["faketest1"]},
		"get_by_code_ok": 	 {"PASS": "PASS", "request": "get", "uri": "http://httpbin.org/get","ok_codes": ["200"]},
		"post_by_code_ok": 	 {"PASS": "PASS", "request": "post", "uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_codes": ["200"]},
		"post_by_code_fail":  {"PASS": "FAIL", "request": "post", "uri": "http://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_codes": ["2001"]},
		"post_by_string_ok":  {"PASS":"PASS", "request": "post","uri": "https://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_strings": ["mus1kpusik2"]},
		"post_by_string_fail":{"PASS":"FAIL","request": "post","uri": "http://httpbin.org/post", "postdata":["test1=mus1kpusik1", "test2=mus1kpusik2"], "ok_strings": ["mus1kpusik3"]},
		"head_by_code":{"PASS":"PASS","request": "head","uri": "http://httpbin.org", "ok_codes":["200"]},
		"json_by_path_ok":{"PASS":"PASS","request": "get","uri": "http://httpbin.org/get", "ok_codes":["200"], "ok_json_nodes":["headers.Host"]},
		"json_by_path_fail":{"PASS":"FAIL","request": "get","uri": "http://httpbin.org/get", "ok_codes":["200"], "ok_json_nodes":["headers.host"]},
		"json_by_val_ok":{"PASS":"PASS","request": "get","uri": "http://httpbin.org/get", "ok_codes":["200"], "ok_json_values":["headers.Host=httpbin.org"]},
		"json_by_val_fail":{"PASS":"FAIL","request": "get","uri": "http://httpbin.org/get", "ok_codes":["200"], "ok_json_values":["headers.host=google.com"]}
	*/

	var jsonTest = []byte(`{
		"get_by_string_ok":  {"PASS":"PASS", "request": "get","uri": "httpsz://httpbin.org/get?musikpusi=1", "ok_strings": ["musikpusi"], "headers":["Content-Fake: faketest"]}

		}`)

	json.Unmarshal(jsonTest, &testdata)

	log.Debugf("tests %+v", testdata)

	inch := make(chan Job, 100)
	outch := make(chan Job, 100)

	for rt, meta := range testdata {
		//configure test job
		exp := ""
		if el, ok := meta["PASS"]; ok {
			if str, ok := el.(string); ok {
				exp = str
			}
		}
		job := NewTestJob(rt, "http", meta, false)
		job.Result["expected"] = exp
		job.Retrace = true

		log.Debugf("job in %+v", job)

		RunTest(job, inch, outch)
	}

	i := 0
	r := true
	for result := range outch {

		v := assert.Equal(t, result.Result["state"], result.Result["expected"], colors.Red(fmt.Sprintf("should be equal %s for %s : %+v", result.Result["expected"], result.Chkid, result)))

		log.Debugf("\nresult %+v\n", result)

		if !v {
			r = false
		}

		i++

		if i == len(testdata) {
			close(outch)
		}
	}
	if r {
		log.Debug(colors.Green("PASS TestHTTP"))
	} else {
		log.Debug(colors.Red("FAIL TestHTTP"))
	}
}

func TestHttpParser(t *testing.T) {

	htmlstring := `*********

<nodder_http_stats>{"http_code":"000","time_total":"0.000","time_lookup":"0.000","time_tfb":"0.000","ssl_code":"0"}</nodder_http_stats>HTTP/1.1 200 OK
Server: nginx
Date: Sun, 10 Jan 2016 04:46:00 GMT
Content-Type: application/json
Content-Length: 245
Connection: keep-alive
Access-Control-Allow-Origin: *
Access-Control-Allow-Credentials: true

{
  "args": {}, 
  "headers": {
    "Accept": "*/*", 
    "Host": "httpbin.org", 
    "Range": "bytes=0-262144", 
    "User-Agent": "Ironpoke/1.0(https://www.ironpoke.com/)"
  }, 
  "origin": "67.161.6.250", 
  "url": "http://httpbin.org/get"
}
<nodder_http_stats>{"http_code":"200","time_total":"0.370","time_lookup":"0.085","time_tfb":"0.370","ssl_code":"0"}</nodder_http_stats>

**********
`

	_, out, err := ParseHttp(htmlstring)
	if err != nil {
		log.Debug("%+s", err.Error())
	}
	log.Debug("%+v", out)

}
