usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "TCP"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"host": "google.com", 
		"port": "80", 
		"timeout": "10", 
		"proto": "ipv4"
	}
}