usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "LDAP""
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"uri": "ldap://ldap.forumsys.com:389", 
		"dn": "cn=read-only-admin,dc=example:password", 
		"timeout": "10"
	}
}