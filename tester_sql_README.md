usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "PING"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		{"engine":"mssql|mssql|postgress", 
		"dsn":"server=127.0.0.1:1433;user id=sa;password=querty;database=test", //depending on engine
		"timeout": "10"}
	}
}

the following are examples of 3 engines

{"engine":"mssql", "dsn":"server=127.0.0.1:1433;user id=sa;password=querty;database=test", "timeout": "10"}
{"engine":"mysql", "dsn":"[username[:password]@][protocol[(address)]]/dbname", "timeout": "10"}
{"engine":"postgress", "dsn":"postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full", "timeout": "10"}
