nodderlib is a collection of tests
so requires curl, ping and traceroute utilities

nodderlib starts starts via nodderlib.RunTest(job Job, inch chan Job, outch chan Job)

where job is structure as such
type Job struct {
	Chkid    string                 `json:"chkid"`
	Chktype  string                 `json:"chktype"`
	Config   map[string]interface{} `json:"config"`
	Retrace  bool                   `json:"retrace"`
	Result   map[string]string
}

Job struct contains definition of test.

- Chkid : string identifier of a Job structure 
- Chktype : string, type of a test inside ["DNS", "HTTP", "TCP", "PING", "TRACE", "FTP", "LDAP", "SQL", "SSH","SMTP"]
- Config: map[string]interface{} this is where test is defined see API ref for specific test.
- Retrace: whether or not run TRACEROUTE on test fail
- Result: map[string]string contains outcomes of a test. [contains status, total_ms, ...]

# API REF

| Test          | Param         | Data Type | Acceptable Values | Notes |
| ------------- |---------------|-----------|-------------------|-------|
| col 3 is      | right-aligned | $1600     |right-aligned      | Blah blah |

