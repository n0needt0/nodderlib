/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/curlerrors"
	"bitbucket.org/n0needt0/keyr"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Jeffail/gabs"
	"net"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type Ht struct {
	Request    string
	Timeout    int
	Uri        string
	UserAgent  string
	Postdata   []string
	Headers    []string
	Cookies    []string
	Ok_codes   []string
	Ok_strings []string
	Ok_headers []string
	Options    []string //remember key => value are sequensed as Options[n] = key, Options[n+1] = value
}

/*
this is for CURL set up
actual job explanation see below
*Get tester supports GET [--get], POST [--form] and HEAD [--head]
* http requests standard curl parameters and options
*by default u can specify on input
timeout in sec [--max-time <sec>]
uri [--url]
postdata -F 'form1=#$1' -F 'form2=2&3' dont use double quotes
headers [-H "header:value" -H "header:value"]
cookies [-b "cookie1:val; cookie2:val"]

and look on output for
code
string
header [-i] //include header in output
cookie
*

default options
-L //follow redirects
--insecure //allow fake certs on servers
--no-keepalive //close as soon as done
--max-redirs 5 //max number of redirects
--post301 //dont conver POST into GET on 301 redirect
--post302 //** on 302
--range 0-262144 //limit bytes to read so people dont point to huge files
--write-out '%{http_code}%{time_total}%{time_namelookup}%{time_starttransfer}%{ssl_verify_result}'


the following optional parameters are supported

-0  use HTTP1.0
-1  use ssl v1
-2  use ssl v2
-3  use ssl v3
-4  use ipv4
-6  use ipv6
-A  user agent (vs default)
--digest Enables HTTP Digest authentication TBD
--cert <certificatefile:password> PEM file that is the private key and the private certificate concatenated!
-v //verbose

*/

const (
	WRITEOUT_HTTP = `<nodder_http_stats>{"http_code":"%{http_code}","time_total":"%{time_total}","time_lookup":"%{time_namelookup}","time_connect":"%{time_connect}","time_tfb":"%{time_starttransfer}","ssl_code":"%{ssl_verify_result}"}</nodder_http_stats>`

	/*		default options
			-L //follow redirects
			--insecure //allow fake certs on servers
			--no-keepalive //close as soon as done
			--max-redirs 5 //max number of redirects
			--post301 //dont conver POST into GET on 301 redirect
			--post302 //** on 302
			--range 0-262144 //limit bytes to read so people dont point to huge files
	*/

	DEFAULTOPTIONS = "-0, -i, -L, --no-keepalive, --max-redirs, 5, --post301, --post302, --range, 0-262144, --anyauth"
)

var HTTP_USER_AGENT = "Nodderlib 1.0"

/*
  this function accepts Job as
	job
	{
	"useragent":"user agent string, if ommited replaced with default",
	"uri":"test uri (required)",
	"request":"GET (default)|POST|HEAD",
	"timeout":"in seconds, default 10sec",
	"ok_codes":"[]string, default ["206","200"]",
	"ok_headers":"[]string i.e. ["header1", "header2"]" both should be in header content"
	"ok_strings":"[]string i.e. ["string1", "string2"]" both should be in content"
	"postdata":"[]string i.e. ["test1=blah", "test2=blah"]",
	"headers":"[]string, example ["Content-Fake:fake","Content-Blah:blah"]""
	"cookies":"[]string"
	"options":"comma delimited list of extra options currently -0  use HTTP1.0
		-1  use ssl v1
		-2  use ssl v2
		-3  use ssl v3
		-4  use ipv4
		-6  use ipv6"
	}
	return
	{
		"response_code"
		"nls_ms"
		"ttfb_ms"
		"total_ms"
		"state"
		"ssl_code"
		"msg"
	}
*/

func f_http() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_http failed on: %+v", x))
			}
		}()

		ht := &Ht{}

		ht.UserAgent = HTTP_USER_AGENT

		valid_requests := map[string]string{"GET": "GET", "POST": "POST", "HEAD": "HEAD", "PUT": "PUT", "DELETE": "DELETE", "HTTP_GET": "GET", "HTTP_POST": "POST", "HTTP_HEAD": "HEAD", "PATCH": "PATCH"}

		k := keyr.New(job.Config)

		var err error

		ht.UserAgent, err = k.GetKeyAsString("useragent")
		if err != nil || ht.UserAgent == "" {
			ht.UserAgent = HTTP_USER_AGENT
		}

		ht.Uri, err = k.GetKeyAsString("uri")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//lets see if we can parse this
		u, err := url.Parse(ht.Uri)
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//encode params
		parameters := u.Query()
		u.RawQuery = parameters.Encode()

		ht.Uri = u.String()

		timeout, err := k.GetKeyAsInt("timeout")
		if err != nil {
			timeout = TESTTIMEOUTDEFSEC
		}

		//get timeout value
		ht.Timeout = timeout

		//get request
		ht.Request = "GET" //default
		v, err := k.GetKeyAsString("request")
		if err == nil && v != "" {
			if v, ok := valid_requests[strings.ToUpper(v)]; ok {
				ht.Request = v
			} else {
				ht.Request = "GET"
			}
		}

		//get optional headers
		ht.Ok_codes = []string{}
		valitf, err := k.GetKeyAsInterface("ok_codes")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Ok_codes = append(ht.Ok_codes, val)
					}
				}
			}
		}

		if len(ht.Ok_codes) < 1 {
			//just use defaults
			ht.Ok_codes = []string{"200", "206"}
		}

		//get optional post data
		ht.Postdata = []string{}
		valitf, err = k.GetKeyAsInterface("postdata")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Postdata = append(ht.Postdata, val)
					}
				}
			}
		}

		//get optional headers
		ht.Headers = []string{"Connection:close"} //this so we recycle connections faster
		valitf, err = k.GetKeyAsInterface("headers")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Headers = append(ht.Headers, val)
					}
				}
			}
		}

		//get optional cookies
		ht.Cookies = []string{} //this so we recycle connections faster
		valitf, err = k.GetKeyAsInterface("cookies")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Cookies = append(ht.Cookies, val)
					}
				}
			}
		}

		//get optional ok headers
		ht.Ok_headers = []string{}
		valitf, err = k.GetKeyAsInterface("ok_headers")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Ok_headers = append(ht.Ok_headers, val)
					}
				}
			}
		}

		//get optional ok headers
		ht.Ok_strings = []string{}
		valitf, err = k.GetKeyAsInterface("ok_strings")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if val, ok := vv.(string); ok {
						ht.Ok_strings = append(ht.Ok_strings, val)
					}
				}
			}
		}

		/************curl magic**************************/

		ht.Options = strings.Split(DEFAULTOPTIONS, ",")
		//add user agent key
		ht.Options = append(ht.Options, "-A")
		ht.Options = append(ht.Options, ht.UserAgent)
		ht.Options = append(ht.Options, "--write-out")
		ht.Options = append(ht.Options, WRITEOUT_HTTP)

		//by default we set not to check ssl certs
		//some would love otherwise
		strictssl := "--insecure"
		s, err := k.GetKeyAsInt("ssl_verifyhost")
		if err == nil && s == 1 {
			strictssl = ""
		} else {

			ht.Options = append(ht.Options, strictssl)
		}

		/*what ever options we may set externally*/
		/*  -0  use HTTP1.0
		-1  use ssl v1
		-2  use ssl v2
		-3  use ssl v3
		-4  use ipv4
		-6  use ipv6
		*/

		validextraoptions := map[string]string{"-1": "-1", "-2": "-2", "-3": "-3", "-4": "-4", "-6": "-6"}

		//get extra options
		valitf, err = k.GetKeyAsInterface("options")
		if err == nil {
			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if oval, ok := vv.(string); ok {
						//check so it is not in already
						add := true
						for _, ai := range ht.Options {
							if ai == oval {
								//already in
								add = false
							}
						}
						//try another option
						if !add {
							continue
						}

						if _, ok := validextraoptions[oval]; ok {
							ht.Options = append(ht.Options, oval) //add to default options
						}
					}
				}
			}
		}

		//trim space incase
		for i, opt := range ht.Options {
			ht.Options[i] = strings.TrimSpace(opt)
		}

		//split port from host as url.parse will return host:port as host
		//and it will breal lookupip

		host, _, err := net.SplitHostPort(u.Host)
		if err != nil {
			//no port found so just use what we had before
			host = u.Host
		}

		job.Config["host"] = host

		_, err = net.LookupIP(host)
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("HTTP error DNS: %s, host: %s, uri: %s", err, host, ht.Uri)
			testch <- job
			return
		}

		test_start := makeTimestamp()

		//some final touches
		//set timeout
		ht.Options = append(ht.Options, "--max-time")
		ht.Options = append(ht.Options, fmt.Sprintf("%d", ht.Timeout))

		//add headers
		for _, hdr := range ht.Headers {
			ht.Options = append(ht.Options, "-H")
			ht.Options = append(ht.Options, hdr)
		}

		switch ht.Request {
		case "POST", "PUT", "PATCH", "DELETE":
			ht.Options = append(ht.Options, "-X")
			ht.Options = append(ht.Options, ht.Request)
			for _, formkeyvalue := range ht.Postdata {
				ht.Options = append(ht.Options, "-d")
				ht.Options = append(ht.Options, formkeyvalue)
			}

		case "HEAD":
			ht.Options = append(ht.Options, "-X")
			ht.Options = append(ht.Options, ht.Request)
			//limit output to head only
			ht.Options = append(ht.Options, "-I")
			ht.Options = append(ht.Options, "--head")

		default:
			ht.Options = append(ht.Options, "-X")
			ht.Options = append(ht.Options, "GET")
		}

		//add cookies
		if len(ht.Cookies) > 1 {
			ht.Options = append(ht.Options, "--cookie")
			ht.Options = append(ht.Options, strings.Join(ht.Cookies, ";"))
		}

		//and at last
		ht.Options = append(ht.Options, "--url")
		ht.Options = append(ht.Options, ht.Uri)

		//default command
		curlcmd := "curl"

		envcurl := os.Getenv(ENV_CURLCMD)
		if envcurl != "" {
			curlcmd = envcurl
		}

		for i, v := range ht.Options {
			log.Debugf("%d=%s", i, v)
		}

		log.Debug(fmt.Sprintf("CURL %s, %+v", curlcmd, ht.Options))

		//Execute curl in external process, with ht.Options make kernel work
		out, err := exec.Command(curlcmd, ht.Options...).Output()
		if err != nil {

			//err.Error() will return in format exit status 7
			//so we chop [exit status ]7 and convert whats left to int then try to get string error

			strerror := err.Error()
			strcode := strings.Replace(strerror, "exit status ", "", -1)
			code, e := strconv.Atoi(strcode)
			if e == nil {
				s := curlerrors.Get_curl_error(code)
				if s != "" {
					strerror = s
				}
			}

			log.Debug(fmt.Sprintf("ExecuteCurl %s", strerror))
			job.Result["msg"] = fmt.Sprintf("HTTP error: %s, uri: %s", strerror, ht.Uri)
			testch <- job
			return
		}

		//in case html body has none
		job.Result["total_ms"] = timeIt(test_start)

		//parce stats in last line of html

		content := string(out[:])

		log.Debugf("GOT CONTENT %s", content)

		body, stats, err := ParseHttp(content)
		if err != nil {
			log.Debug(fmt.Sprintf("ParseHttp %s", err.Error()))
			job.Result["msg"] = fmt.Sprintf("HTTP error: %s, uri: %s", err.Error(), ht.Uri)
			testch <- job
			return
		}

		/************end curl magic**********************/

		job.Result["response_code"] = stats.HttpCode
		//we got evertyhing in seconds
		//lets parse, convert to msec *1000
		//and convert em back again to strings

		job.Result["nsl_ms"] = "0"
		if flt, err := strconv.ParseFloat(stats.TimeLookupS, 64); err == nil {
			job.Result["nsl_ms"] = fmt.Sprintf("%d", int(flt*1000))
		}

		job.Result["tfb_ms"] = "0"
		if flt, err := strconv.ParseFloat(stats.TimeTfbS, 64); err == nil {
			job.Result["tfb_ms"] = fmt.Sprintf("%d", int(flt*1000))
		}

		if flt, err := strconv.ParseFloat(stats.TimeTotalS, 64); err == nil {
			job.Result["total_ms"] = fmt.Sprintf("%d", int(flt*1000))
		}

		job.Result["ssl_code"] = stats.SslCode

		if flt, err := strconv.ParseFloat(stats.TimeToConnectS, 64); err == nil {
			job.Result["con_ms"] = fmt.Sprintf("%d", int(flt*1000))
		}

		for _, val := range ht.Ok_codes {
			if val == job.Result["response_code"] {
				job.Result["state"] = STATE_UP
			}
		}

		//check strings
		for _, ok_string := range ht.Ok_strings {
			if !strings.Contains(body, ok_string) {
				job.Result["msg"] = fmt.Sprintf("string \"%s\" not found", ok_string)
				job.Result["state"] = STATE_DOWN
				testch <- job
				return
			}
		}

		//check header
		for _, ok_header := range ht.Ok_headers {
			if !strings.Contains(body, ok_header) {
				//we are missing one
				job.Result["msg"] = fmt.Sprintf("header \"%s\" not found", ok_header)
				job.Result["state"] = STATE_DOWN
				testch <- job
				return
			}
		}

		//potential json will be after and including first {
		jsonbody := ""

		//check json nodes
		valitf, err = k.GetKeyAsInterface("ok_json_nodes")
		if err == nil {

			t := strings.SplitAfterN(body, "{", 2)
			for _, v := range t {
				jsonbody = "{" + v //we grab a reminder after 1st {
			}

			log.Debugf("JSON received %+v ", jsonbody)

			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if nodepath, ok := vv.(string); ok {
						log.Debugf("testing path %s", nodepath)

						jsonParsed, err := gabs.ParseJSON([]byte(jsonbody))
						if err != nil {
							//we are missing one
							job.Result["msg"] = fmt.Sprintf("%s \"%s\"", err.Error(), jsonbody)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}

						log.Debugf("received json %+v", jsonParsed)

						//otherwise
						exists := jsonParsed.ExistsP(nodepath)
						if exists != true {
							//we are missing one
							job.Result["msg"] = fmt.Sprintf("path does not exists \"%s\"", nodepath)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}
					}
				}
			}
		}

		//check json nodes
		valitf, err = k.GetKeyAsInterface("ok_json_values")
		if err == nil {

			t := strings.SplitAfterN(body, "{", 2)
			for _, v := range t {
				jsonbody = "{" + v //we grab a reminder after 1st {
			}

			log.Debugf("JSON received %+v ", jsonbody)

			if elarr, ok := valitf.([]interface{}); ok {
				for _, vv := range elarr {
					if nodepathvalue, ok := vv.(string); ok {

						//remember nodepah=nodevalue
						s := strings.Split(nodepathvalue, "=")
						if len(s) != 2 {
							job.Result["msg"] = fmt.Sprintf("invalid json nodepath=value \"%s\"", nodepathvalue)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}

						nodepath := s[0]
						expectedvalue := s[1]

						jsonParsed, err := gabs.ParseJSON([]byte(jsonbody))
						if err != nil {
							job.Result["msg"] = fmt.Sprintf("bad json \"%s\"", jsonbody)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}

						log.Debugf("received json %+v", jsonParsed)

						//otherwise
						nodevalue, ok := jsonParsed.Path(nodepath).Data().(string)
						if !ok {
							job.Result["msg"] = fmt.Sprintf("path does not exists \"%s\"", nodepath)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}

						if nodevalue != expectedvalue {
							job.Result["msg"] = fmt.Sprintf("values dont match nodevalue != expectedvalue \"%s\" != \"%s\"", nodevalue, expectedvalue)
							job.Result["state"] = STATE_DOWN
							testch <- job
							return
						}
					}
				}
			}
		}

		//and finally
		testch <- job
		return
	}
}

type HttpOut struct {
	HttpCode       string `json:"http_code"`
	TimeTotalS     string `json:"time_total"`
	TimeLookupS    string `json:"time_lookup"`
	TimeTfbS       string `json:"time_tfb"`
	SslCode        string `json:"ssl_code"`
	TimeToConnectS string `json:"time_connect"`
}

func ParseHttp(s string) (string, *HttpOut, error) {

	var err error

	//we only care for last line

	lines := strings.Split(s, "<nodder_http_stats>")

	bodypart := lines[0]

	lines = strings.Split(lines[len(lines)-1], "</nodder_http_stats>")

	str := []byte(lines[0])

	//lines[len(lines)-1]
	res := &HttpOut{}

	err = json.Unmarshal(str, res)
	if err != nil {
		return bodypart, res, errors.New(fmt.Sprintf("can not parse http output %s, %s", str, err.Error()))
	}
	return bodypart, res, nil
}
