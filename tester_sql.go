/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb" //{"engine":"mssql", "dsn":"server=127.0.0.1:1433;user id=sa;password=querty;database=test"}
	_ "github.com/go-sql-driver/mysql"   //{"engine":"mysql", "dsn":"[username[:password]@][protocol[(address)]]/dbname"}
	_ "github.com/lib/pq"                //{"engine":"postgres", "dsn":"postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full"}
	"time"
)

var dbengine = map[string]string{"mysql": "mysql",
	"postgres": "postgres",
	"mssql":    "mssql"}

/*this function tests connection to mysql database
it allows to execute submitted query and find
result in returned output
job config as follows

{"engine": "mysql", "dsn": "root:n0needt0z@tcp(127.0.0.1:3306)/mysql", "timeout": "10"}

the following engines and dsns are supported example:
{"engine":"mssql", "dsn":"server=127.0.0.1:1433;user id=sa;password=querty;database=test", "timeout": "10"}
{"engine":"mysql", "dsn":"[username[:password]@][protocol[(address)]]/dbname", "timeout": "10"}
{"engine":"postgres", "dsn":"postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full", "timeout": "10"}

returns
{
"state"
"total_ms"
"msg"
}
*/

func f_sql() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_mysql failed on: %+v , job: %+v", x, job))
			}
		}()

		k := keyr.New(job.Config)

		//validate host
		engine, err := k.GetKeyAsString("engine")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		driver := ""

		if d, ok := dbengine[engine]; !ok {
			job.Result["msg"] = errors.New("Bad db engine specified").Error()
			testch <- job
			return
		} else {
			driver = d
		}

		dsn, err := k.GetKeyAsString("dsn")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if dsn == "" {
			job.Result["msg"] = errors.New("No DSN specified").Error()
			testch <- job
			return
		}

		test_start := makeTimestamp()

		db, err := sql.Open(driver, dsn)
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}
		defer db.Close()

		time.Sleep(time.Duration(1) * time.Second)

		// Open doesn't open a connection. Validate DSN data:
		err = db.Ping()
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		job.Result["state"] = STATE_UP
		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
	}
}
