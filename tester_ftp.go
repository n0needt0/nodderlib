/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/curlerrors"
	"bitbucket.org/n0needt0/keyr"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func f_ftp() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_ftp failed on:  %+v, %+v", x, job))
			}
		}()

		k := keyr.New(job.Config)

		//get uri
		uri, err := k.GetKeyAsString("uri")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		var options []string

		//lets see if we can parse this uri
		_, err = url.Parse(uri)
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		timeout, err := k.GetKeyAsInt("timeout")
		if err != nil {
			timeout = TESTTIMEOUTDEFSEC
		}

		options = append(options, "--url")
		options = append(options, uri)

		options = append(options, "--max-time")
		options = append(options, fmt.Sprintf("%d", timeout))

		s, err := k.GetKeyAsInt("ssl_verifyhost")
		if err == nil && s == 1 {
			//nothing to do
		} else {
			options = append(options, "--insecure")
		}

		//default command
		curlcmd := "curl"

		envcurl := os.Getenv(ENV_CURLCMD)
		if envcurl != "" {
			curlcmd = envcurl
		}

		test_start := makeTimestamp()
		timeout = timeout

		//Execute curl in external process
		out, err := exec.Command(curlcmd, options...).Output()
		if err != nil {
			//err.Error() will return in format exit status 7
			//so we chop [exit status ]7 and convert whats left to int then try to get string error

			strerror := err.Error()
			strcode := strings.Replace(strerror, "exit status ", "", -1)
			code, e := strconv.Atoi(strcode)
			if e == nil {
				s := curlerrors.Get_curl_error(code)
				if s != "" {
					strerror = s
				}
			}

			log.Debug(fmt.Sprintf("ExecuteCurl %s", strerror))

			job.Result["msg"] = fmt.Sprintf("FTP error: %s, uri: %s", strerror, uri)
			testch <- job
			return
		}
		if string(out[:]) != "" {
			job.Result["state"] = STATE_UP
		}

		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
		return
	}
}
