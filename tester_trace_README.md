usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "TCP"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"host": "localhost", 
		"options": "comma delimited list of traceroute options"
					by default [-I, -n, -m 16, -q 1, -w 45]
	}
}