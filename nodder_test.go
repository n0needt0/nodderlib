/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nodderlib

import (
	//	"fmt"
	//	"github.com/stretchr/testify/assert"
	"testing"
)

//this is a test job
//shared across all tests

func NewTestJob(chkid string, chktype string, config map[string]interface{}, retrace bool) Job {
	return Job{
		Chkid:    chkid,
		Chktype:  chktype,
		Config:   config,
		Retrace:  retrace,
		Runtrace: false,
		Result:   map[string]string{"state": STATE_DOWN},
	}
}

func TestNodder(t *testing.T) {
}
