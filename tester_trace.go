/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	valid "github.com/asaskevich/govalidator"
	"net"
	"net/url"
	"os/exec"
	"runtime"
	"strings"
)

//this where is route stored
type TraceRouteHop struct {
	Ip     string
	TimeMs string
}

func NewTraceRouteHop(Ip string, TimeMs string) TraceRouteHop {
	return TraceRouteHop{Ip, TimeMs}
}

func f_trace() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_trace failed on: %+v %+v", x, job))
			}
		}()
		k := keyr.New(job.Config)

		timeout := TRACEROUTETIMEOUT

		//validate host
		host, err := k.GetKeyAsString("host")
		if err != nil {
			job.Result["traceroute"] = err.Error()
			testch <- job
			return
		}

		if host == "" {
			job.Result["msg"] = errors.New("No host specified").Error()
			testch <- job
			return
		}

		/*[-I|-T use icmp|tcp], -n no name resolution, -m max 16 hops -q max 1 query per hop*/
		defaultoptions := fmt.Sprintf("-I, -n, -m 16, -q 1, -w %d", timeout)
		var options []string

		v, err := k.GetKeyAsString("options")
		if err == nil && v != "" {
			options = strings.Split(v, ",")
		} else {
			job = job.setcfgdefault("options", defaultoptions)
			options = strings.Split(defaultoptions, ",")
		}

		for i, option := range options {
			options[i] = strings.TrimSpace(option)
		}

		_, err = net.LookupIP(host)
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("TRACE error DNS: %s, host: %s", err, host)
			testch <- job
			return
		}

		test_start := makeTimestamp()

		//Execute sys traceroute in external process, with options make kernel work
		out, err := exec.Command("traceroute", append(options, host)...).Output()
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("TRACEROUTE error: %s, host: %s", err.Error(), host)
			testch <- job
			return
		}
		job.Result["total_ms"] = timeIt(test_start)

		//split output in lines
		lines := strings.Split(string(out[:]), "\n")

		//evaluate first line, this is where ip will be in () grep it
		destinationip := ""
		left := strings.Split(lines[0], "(")
		if len(left) > 1 {
			right := strings.Split(left[1], ")")
			if len(right) > 1 && valid.IsIP(strings.TrimSpace(right[0])) {
				destinationip = strings.TrimSpace(right[0])
				//found our destinations
			}
		}

		if destinationip == "" {
			//boss this is weird! i cant get destination ip from first line of traceroute output
			job.Result["msg"] = fmt.Sprintf("TRACEROUTE error can not figure out what destination is!: from %s", lines[0])
			testch <- job
			return
		}

		//need this to build md5
		Hops := []string{}

		Route := []TraceRouteHop{}
		done := false

		for _, line := range lines[1:] {
			linedata := strings.Split(strings.TrimSpace(line), " ")
			hopip := "*"
			hoptime := ""
			done = false
			//split will generate bunch of empties
			for _, el := range linedata[1:] {
				el = strings.TrimSpace(el)
				if "" != el {
					//we only want what is iP
					if valid.IsIP(el) {
						hopip = el
						if destinationip == hopip {
							//got to the destination
							done = true
						}
					}

					if hopip != "*" && valid.IsFloat(el) {
						//it may be float here, only after ip found
						hoptime = el
					}
				}
			}

			Route = append(Route, NewTraceRouteHop(hopip, hoptime))
			if done {
				//destination reached
				break
			}
		}

		//build md5 so we know if changes were made later
		for _, hop := range Route {
			Hops = append(Hops, hop.Ip)
		}

		md5str := fmt.Sprintf("%x", md5.Sum([]byte(fmt.Sprintf("%v", Hops))))
		md5str = md5str
		route, _ := json.Marshal(Route)
		route = route

		//got to our destination
		if done == true {
			job.Result["state"] = STATE_UP
		}

		job.Result["hops"] = string(route)
		job.Result["hopcount"] = fmt.Sprintf("%d", len(Route))
		job.Result["routemd5"] = md5str
		job.Result["tracedata"] = fmt.Sprintf(string(out[:]))

		testch <- job
		return
	}
}

func f_retrace() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_retrace failed on: %+v", x))
				runtime.Goexit()
			}
		}()

		log.Debugf("IN RETRACE %+v", job)

		k := keyr.New(job.Config)

		//validate host
		host := ""

		host, _ = k.GetKeyAsString("host")

		//in case uri is specified
		if host == "" {
			//get uri
			uri, err := k.GetKeyAsString("uri")
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			//lets see if we can parse this
			u, err := url.Parse(uri)
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			host = u.Host
		}

		if host == "" {
			job.Result["msg"] = errors.New("No host specified").Error()
			testch <- job
			return
		}

		timeout := TRACEROUTETIMEOUT

		/*[-I|-T use icmp|tcp], -n no name resolution, -m max 16 hops -q max 1 query per hop*/
		defaultoptions := fmt.Sprintf("-I, -n, -m 16, -q 1, -w %d", timeout)
		var options []string

		v, err := k.GetKeyAsString("options")
		if err == nil && v != "" {
			options = strings.Split(v, ",")
		} else {
			job = job.setcfgdefault("options", defaultoptions)
			options = strings.Split(defaultoptions, ",")
		}

		for i, option := range options {
			options[i] = strings.TrimSpace(option)
		}

		_, err = net.LookupIP(host)
		if err != nil {
			job.Result["traceroute"] = err.Error()
			testch <- job
			return
		}

		log.Debugf("IN TRACEROUTE %+v %+s", options, host)

		//Execute sys traceroute in external process, with options make kernel work
		out, err := exec.Command("traceroute", append(options, host)...).Output()
		if err != nil {
			job.Result["traceroute"] = fmt.Sprintf("error: %s %s, host: %s", err.Error(), " are you root?", host)
			testch <- job
			return
		}

		job.Result["traceroute"] = fmt.Sprintf(string(out[:]))

		testch <- job
		return
	}
}
