/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	colors "bitbucket.org/n0needt0/libs"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTCP(t *testing.T) {

	log.Debug(colors.Cyan("TestTCP"))

	testdata := map[string]map[string]interface{}{
		"1": {"host": "google.com", "port": "80", "timeout": "10", "proto": "ipv4", "PASS": STATE_UP},
		"2": {"host": "google.com", "port": "443", "timeout": "10", "proto": "ipv4", "PASS": STATE_UP},
		"3": {"host": "google.com", "port": "", "timeout": "10", "proto": "ipv4", "PASS": STATE_DOWN},
		//"4": {"host": "google.com", "port": "80", "timeout": "10", "proto": "tcp6", "PASS": STATE_UP},
		//	"5": {"host": "google.com", "port": "443", "timeout": "10", "proto": "tcp6", "PASS": STATE_UP},
		//	"6": {"host": "google.com", "port": "", "timeout": "10", "proto": "tcp6", "PASS": STATE_DOWN},
		"7": {"host": "", "port": "80", "timeout": "10", "proto": "ipv4", "PASS": STATE_DOWN},
		"8": {"host": "google.com", "port": "80", "timeout": "100", "proto": "ipv4", "PASS": STATE_UP},
	}

	log.Debugf("%+v", testdata)

	inch := make(chan Job, 100)
	outch := make(chan Job, 100)

	for k, meta := range testdata {

		exp := ""
		if el, ok := meta["PASS"]; ok {
			if str, ok := el.(string); ok {
				exp = str
			}
		}
		//configure test job
		job := NewTestJob(k, "TCP", meta, false)
		job.Result["expected"] = exp

		log.Debug("job in %+v", job)

		RunTest(job, inch, outch)
	}

	i := 0
	r := true
	for result := range outch {

		v := assert.Equal(t, result.Result["state"], result.Result["expected"], colors.Red(fmt.Sprintf("should be equal %s for %s : %+v", result.Result["expected"], result.Chkid, result)))

		log.Debugf("result %+v", result)

		if !v {
			r = false
		}

		i++

		if i == len(testdata) {
			close(outch)
		}
	}
	if r {
		log.Debug(colors.Green("PASS TestTCP"))
	} else {
		log.Debug(colors.Red("FAIL TestTCP"))
	}
}
