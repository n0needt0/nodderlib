/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	colors "bitbucket.org/n0needt0/libs"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDNS(t *testing.T) {

	log.Debug(colors.Cyan("TestDNS"))

	test_dns_records := map[string]string{"CRAZYREC": STATE_UP, "DNS_A": STATE_UP, "DNS_MX": STATE_UP, "DNS_ANY": STATE_UP}

	inch := make(chan Job, 100)
	outch := make(chan Job, 100)

	for rt, expected := range test_dns_records {
		meta := map[string]interface{}{"host": "google.com", "record_type": rt, "timeout": 10}
		//configure test job
		job := NewTestJob("1", "DNS", meta, false)
		job.Result["expected"] = expected
		RunTest(job, inch, outch)
	}
	i := 0
	r := true
	for result := range outch {

		v := assert.Equal(t, result.Result["state"], result.Result["expected"], colors.Red(fmt.Sprintf("should be equal %s for %s", result.Result["expected"], result.Chkid)))

		log.Debugf("result %+v", result)

		if !v {
			r = false
		}

		i++

		if i == len(test_dns_records) {
			close(outch)
		}
	}
	if r {
		log.Debug(colors.Green("PASS TestDNS"))
	} else {
		log.Debug(colors.Red("FAIL TestDNS"))
	}
}
