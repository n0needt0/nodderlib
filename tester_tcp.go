/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"errors"
	"fmt"
	"net"
	"time"
)

func f_tcp() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_tcp failed on:  %+v, %+v", x, job))
			}
		}()
		k := keyr.New(job.Config)

		//validate host
		host, err := k.GetKeyAsString("host")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if host == "" {
			job.Result["msg"] = errors.New("No host specified").Error()
			testch <- job
			return
		}

		//get port
		port, err := k.GetKeyAsString("port")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if port == "" {
			job.Result["msg"] = errors.New("No port specified").Error()
			testch <- job
			return
		}

		timeout := GetTimeout(k, job)

		//get networking protocol to use
		proto := "tcp4"
		v, err := k.GetKeyAsString("proto")
		if err != nil {
			job = job.setcfgdefault("proto", proto)
		}

		if v == "tcp" || v == "tcp4" || v == "tcp6" {
			proto = v
			job = job.setcfgvalue("proto", v)
		} else {
			job = job.setcfgdefault("proto", proto)
		}

		test_start := makeTimestamp()

		c, err := net.DialTimeout(proto, fmt.Sprintf("%s:%s", host, port), time.Duration(timeout)*time.Second)
		if err != nil {
			job.Result["msg"] = err.Error()
		} else {
			c.Close()
			job.Result["state"] = STATE_UP
		}
		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
		return
	}
}
