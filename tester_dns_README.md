usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype":"DNS"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
	"host": "google.com", 
	"record_type": one of "DNS_A", "DNS_CNAME", "DNS_HINFO", "DNS_MX", "DNS_NS", "DNS_PTR", "DNS_SOA", "DNS_TXT", "DNS_AAAA", "DNS_SRV", "DNS_NAPTR", "DNS_A6", "DNS_ALL", "DNS_ANY", 
	"timeout": 10
	}
}