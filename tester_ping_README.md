usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "PING"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"host": "www.google.com", 
		"timeout": 20, //sec
		"size": 33, //packet size
		"packets": 5, //packetcount
		"ok_ploss": 75 //loss limit
		"options":"//ping options"
	}
}