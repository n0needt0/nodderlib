usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": "SSH"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"host": "localhost", 
		"port": "22", 
		"username": "user", 
		"password": "password", 
		"key":"private key id_rsa or id_dsa"
		"timeout": "10", 
		"proto": "ipv4"
	}
}