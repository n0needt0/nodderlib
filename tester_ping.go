/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"errors"
	"fmt"
	"net"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

func f_ping() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_ping failed on: %+v %+v", x, job))
			}
		}()
		k := keyr.New(job.Config)

		//validate host
		host, err := k.GetKeyAsString("host")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if host == "" {
			job.Result["msg"] = errors.New("No host specified").Error()
			testch <- job
			return
		}

		//get ploss
		ok_ploss, err := k.GetKeyAsInt("ok_ploss")
		if err != nil || ok_ploss < 0 || ok_ploss > 100 {
			ok_ploss = 0
			job = job.setcfgdefault("ok_ploss", "0")
		}

		//get packetcount
		packets, err := k.GetKeyAsInt("packets")
		if err != nil || packets < 1 || packets > 10 {
			packets = 4 //DEFAULT_PACKET_CNT
			job = job.setcfgdefault("packets", fmt.Sprintf("%d", packets))
		}

		//get packetsize
		packetsize, err := k.GetKeyAsInt("packetsize")
		if err != nil || packetsize < 10 || packetsize > 109 {
			packetsize = 34 //DEFAULT_PACKET_SIZE
			job = job.setcfgdefault("packetsize", fmt.Sprintf("%d", packetsize))
		}

		/*standard man ping options*/
		options := strings.Split(fmt.Sprintf("-c %d, -s %d, -w %d", packets, packetsize, TESTTIMEOUTDEFSEC), ",")

		/*what ever options we may set externally*/
		v, err := k.GetKeyAsString("options")
		if err == nil && v != "" {
			opt := strings.Split(v, ",")
			//check inside that options are not colliding with defautl
			for _, o := range opt {
				if strings.Contains(o, "-c ") || strings.Contains(o, "-s ") || strings.Contains(o, "-w ") {
					options = append(options, o) //add to default options
				}
			}
		}

		//trim space incase
		for i, option := range options {
			options[i] = strings.TrimSpace(option)
		}

		_, err = net.LookupIP(host)
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("PING error DNS: %s, host: %s", err, host)
			testch <- job
			return
		}

		test_start := makeTimestamp()

		//default command
		pingcmd := "ping"

		envping := os.Getenv(ENV_PINGCMD)
		if envping != "" {
			pingcmd = envping
		}

		log.Debugf("PING %s, %+v %s", pingcmd, options, host)

		//Execute ping in external process, with options make kernel work
		out, err := exec.Command(pingcmd, append(options, host)...).Output()
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("PING error: %s, host: %s", err.Error(), host)
			testch <- job
			return
		}
		job.Result["total_ms"] = timeIt(test_start)

		job.Result["raw"] = string(out[:])

		//parce output
		pingout, err := ParsePingOut(string(out[:]))
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		for k, v := range pingout {
			job.Result[k] = v
		}

		loss, err := strconv.Atoi(job.Result["loss_p"])
		if err != nil {
			testch <- job
			return
		}

		if loss <= ok_ploss {
			job.Result["state"] = STATE_UP
		} else {
			job.Result["msg"] = fmt.Sprintf("PING error: host %s, packet loss %d percent", host, loss)
		}
		testch <- job
		return
	}
}

/*
 Parses the `ping_output` string into a dictionary containing the following
    fields:
*/

func ParsePingOut(s string) (map[string]string, error) {

	out := map[string]string{"packets_sent": "0", "packets_received": "0", "percent_loss": "100", "rtt_min": "", "rtt_max": "", "rtt_avg": "", "jitter": ""}

	re := regexp.MustCompile(`PING ([a-zA-Z0-9.\-]+)`)
	host := re.FindStringSubmatch(s)
	if len(host) < 2 {
		return out, errors.New(fmt.Sprintf("can not parse ping output %s", s))
	}

	re = regexp.MustCompile(`(\d+) packets transmitted, (\d+) received, (\d+)% packet loss, time (\d+)ms`)
	prop := re.FindStringSubmatch(s)
	if len(prop) < 5 {
		return out, errors.New(fmt.Sprintf("can not parse ping output %s", s))
	}

	out["sent"] = prop[1]
	out["sent_ok"] = prop[2]
	out["loss_p"] = prop[3]

	re = regexp.MustCompile(`rtt min/avg/max/mdev = (\d+.\d+)/(\d+.\d+)/(\d+.\d+)/(\d+.\d+) ms`)
	prop = re.FindStringSubmatch(s)
	if len(prop) < 5 {
		return out, errors.New(fmt.Sprintf("can not parse ping output %s", s))
	}

	out["rt_min"] = prop[1]
	out["rt_avg"] = prop[2]
	out["rt_max"] = prop[3]
	out["rt_jitter"] = prop[4]

	return out, nil
}
