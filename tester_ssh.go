/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"errors"
	"fmt"
	"golang.org/x/crypto/ssh"
)

func f_ssh() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_ssh failed on:  %+v, %+v", x, job))
			}
		}()
		k := keyr.New(job.Config)

		//validate host
		host, err := k.GetKeyAsString("host")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if host == "" {
			job.Result["msg"] = errors.New("No host specified").Error()
			testch <- job
			return
		}

		//get port
		port, err := k.GetKeyAsString("port")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if port == "" {
			port = "22"
		}

		//get user
		user, err := k.GetKeyAsString("username")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if user == "" {
			job.Result["msg"] = errors.New("No user specified").Error()
			testch <- job
			return
		}

		//get password
		password := ""
		password, _ = k.GetKeyAsString("password")

		//get key
		key := ""
		key, _ = k.GetKeyAsString("key")

		if user == "" && password == "" && key == "" {
			job.Result["msg"] = "password or key must be set"
			testch <- job
			return
		}

		//get networking protocol to use
		proto := "tcp"
		v, err := k.GetKeyAsString("proto")
		if err != nil {
			job = job.setcfgdefault("proto", proto)
		}

		if v == "tcp" || v == "tcp4" || v == "tcp6" {
			proto = v
			job = job.setcfgvalue("proto", v)
		} else {
			job = job.setcfgdefault("proto", proto)
		}

		test_start := makeTimestamp()

		//set authentication
		config := &ssh.ClientConfig{}

		if key != "" {
			//use pkey
			// Create the Signer for this private key.
			signer, err := ssh.ParsePrivateKey([]byte(key))
			if err != nil {
				job.Result["msg"] = fmt.Sprintf("key parse: %s", err.Error())
				testch <- job
				return
			}

			config = &ssh.ClientConfig{
				User: user,
				Auth: []ssh.AuthMethod{
					// Use the PublicKeys method for remote authentication.
					ssh.PublicKeys(signer),
				},
			}

		} else {
			//use password
			config = &ssh.ClientConfig{
				User: user,
				Auth: []ssh.AuthMethod{
					ssh.Password(password),
				},
			}
		}

		client, err := ssh.Dial(proto, fmt.Sprintf("%s:%s", host, port), config)
		if err != nil {
			job.Result["msg"] = fmt.Sprintf("dial: %s", err.Error())
			testch <- job
			return
		}

		session, err := client.NewSession()
		if err != nil {
			job.Result["msg"] = err.Error()
		} else {
			session.Close()
			job.Result["state"] = STATE_UP
		}

		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
		return
	}
}
