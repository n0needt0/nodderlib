/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"crypto/tls"
	"fmt"
	"net"
	"net/smtp"
	"net/url"
	"strings"
)

func f_smtp() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debugf("f_smtp failed on:  %+v, %+v", x, job)
			}
		}()

		k := keyr.New(job.Config)

		//get uri
		uri, err := k.GetKeyAsString("uri")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//force certificates
		skipverify := true
		cb, err := k.GetKeyAsInt("ssl_verifyhost")
		if err == nil && cb == 1 {
			skipverify = false
		}

		//lets see if we can parse this uri
		u, err := url.Parse(uri)
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		log.Debug("parsed URI %+v", u)

		user, pwd := "", ""
		doauth := false

		if u.User != nil {
			user = u.User.Username()
			pwd, _ = u.User.Password()

			if user != "" && pwd != "" {
				doauth = true
			}
		}

		host := ""
		port := ""
		host, port, err = net.SplitHostPort(u.Host)

		if err != nil {
			//no port found so just use what we had before
			host = u.Host
		}

		if host == "" {
			job.Result["msg"] = fmt.Sprintf("Invalid host %s", uri)
			testch <- job
			return
		}

		timeout, err := k.GetKeyAsInt("timeout")
		if err != nil {
			timeout = TESTTIMEOUTDEFSEC
		}

		test_start := makeTimestamp()
		timeout = timeout

		//first we figure out if it will be secure or none
		switch strings.ToLower(u.Scheme) {
		case "smtp", "starttls":
			if port == "" {
				port = "25" //default smtp
			}

			//get client
			c, err := smtp.Dial(fmt.Sprintf("%s:%s", host, port))
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			err = c.Hello("localhost")
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			if strings.ToLower(u.Scheme) == "starttls" {

				// TLS config
				tlsconfig := &tls.Config{
					InsecureSkipVerify: skipverify,
					ServerName:         host,
				}

				err := c.StartTLS(tlsconfig)
				if err != nil {
					job.Result["msg"] = err.Error()
					testch <- job
					return
				}

				_, ok := c.TLSConnectionState()

				if !ok {
					job.Result["msg"] = "TLS connection state is invalid"
					testch <- job
					return
				}
			}

			if doauth {
				a := smtp.PlainAuth(
					"",
					user,
					pwd,
					host,
				)

				err := c.Auth(a)
				if err != nil {
					job.Result["msg"] = err.Error()
					testch <- job
					return
				}
			}

			job.Result["state"] = STATE_UP
			c.Close()

		case "smtps":
			if port == "" {
				port = "587" //default smtps
			}

			// TLS config
			tlsconfig := &tls.Config{
				InsecureSkipVerify: skipverify,
				ServerName:         host,
			}

			log.Debug(fmt.Sprintf("Dial SMTPS: %s:%s", host, port))

			conn, err := tls.Dial("tcp", fmt.Sprintf("%s:%s", host, port), tlsconfig)
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			c, err := smtp.NewClient(conn, host)
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			err = c.Hello("localhost")
			if err != nil {
				job.Result["msg"] = err.Error()
				testch <- job
				return
			}

			if doauth {
				a := smtp.PlainAuth(
					"",
					user,
					pwd,
					host,
				)
				err := c.Auth(a)
				if err != nil {
					job.Result["msg"] = err.Error()
					testch <- job
					return
				}
			}

			job.Result["state"] = STATE_UP
			c.Close()

		default:
			job.Result["msg"] = "Invalid schema support smtp and smtps only"
			testch <- job
			return
		}

		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
		return
	}
}
