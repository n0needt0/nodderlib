usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype":"FTP"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		"uri": "ftp://test:123@speedtest.tele2.net/", 
		"timeout": "10"
		}
}