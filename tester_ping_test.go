/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	colors "bitbucket.org/n0needt0/libs"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPING(t *testing.T) {

	log.Debug(colors.Cyan("TestPING"))

	testdata := map[string]map[string]interface{}{
		"1": {"host": "23.249.50.31", "timeout": 2, "size": 34, "packets": 5, "ok_ploss": 2.5, "PASS": STATE_UP},
		"2": {"host": "www.google.com", "timeout": 20, "size": 33, "packets": 5, "PASS": STATE_UP},
		"3": {"host": "", "timeout": 20, "size": 33, "packets": 10, "PASS": STATE_DOWN},
	}

	inch := make(chan Job, 100)
	outch := make(chan Job, 100)

	for rt, meta := range testdata {
		//configure test job

		exp := ""
		if el, ok := meta["PASS"]; ok {
			if str, ok := el.(string); ok {
				exp = str
			}
		}
		job := NewTestJob(rt, "PING", meta, false)
		job.Result["expected"] = exp

		RunTest(job, inch, outch)
	}

	i := 0
	r := true
	for result := range outch {

		v := assert.Equal(t, result.Result["state"], result.Result["expected"], colors.Red(fmt.Sprintf("should be equal %s for %s : %+v", result.Result["expected"], result.Chkid, result)))

		log.Debugf("result %+v", result)

		if !v {
			r = false
		}

		i++

		if i == len(testdata) {
			close(outch)
		}
	}
	if r {
		log.Debug(colors.Green("PASS TestPING"))
	} else {
		log.Debug(colors.Red("FAIL TestPING"))
	}
}

func TestPingParser(t *testing.T) {

	pingstring := `PING www.facebook.com (173.252.89.132) 56(84) bytes of data.
64 bytes from edge-star-mini-shv-06-atn1.facebook.com (173.252.89.132): icmp_req=1 ttl=77 time=53.2 ms
64 bytes from edge-star-mini-shv-06-atn1.facebook.com (173.252.89.132): icmp_req=2 ttl=77 time=53.0 ms
64 bytes from edge-star-mini-shv-06-atn1.facebook.com (173.252.89.132): icmp_req=3 ttl=77 time=54.2 ms
64 bytes from edge-star-mini-shv-06-atn1.facebook.com (173.252.89.132): icmp_req=4 ttl=77 time=52.6 ms

--- star-mini.c10r.facebook.com ping statistics ---
5 packets transmitted, 7 received, 3% packet loss, time 3004ms
rtt min/avg/max/mdev = 52.690/53.319/54.267/0.610 ms`

	out, err := ParsePingOut(pingstring)
	if err != nil {
		log.Debugf("%+s", err.Error())
	}
	log.Debugf("%+v", out)

}
