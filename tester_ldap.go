/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/curlerrors"
	"bitbucket.org/n0needt0/keyr"
	"fmt"
	"net/url"
	"os/exec"
	"strconv"
	"strings"
)

//f_ldap uses curl to do ldap connection
//this is tested on openldap with as follows
//curl -u cn=admin,dc=nodomain:password "ldap://localhost:389"

func f_ldap() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_ldap failed on:  %+v, %+v", x, job))
			}
		}()

		k := keyr.New(job.Config)

		//get uri
		uri, err := k.GetKeyAsString("uri")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//lets see if we can parse this uri
		_, err = url.Parse(uri)
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//get login
		dn, err := k.GetKeyAsString("dn")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		if dn == "" {
			job.Result["msg"] = "Ldap dn (login) required in format of your server, example cn=admin,dc=domain:password "
			testch <- job
			return
		}

		timeout, err := k.GetKeyAsInt("timeout")
		if err != nil {
			timeout = TESTTIMEOUTDEFSEC
		}

		test_start := makeTimestamp()
		timeout = timeout

		//Execute curl in external process
		out, err := exec.Command("curl", "-u", dn, uri, "--max-time", fmt.Sprintf("%d", timeout), "-k").Output()
		if err != nil {
			//err.Error() will return in format exit status 7
			//so we chop [exit status ]7 and convert whats left to int then try to get string error

			strerror := err.Error()
			strcode := strings.Replace(strerror, "exit status ", "", -1)
			code, e := strconv.Atoi(strcode)
			if e == nil {
				s := curlerrors.Get_curl_error(code)
				if s != "" {
					strerror = s
				}
			}

			log.Debug(fmt.Sprintf("ExecuteCurl %s", strerror))

			job.Result["msg"] = fmt.Sprintf("LDAP error: %s, uri: %s, dn %s", strerror, uri, dn)
			testch <- job
			return
		}

		if string(out[:]) != "" {
			job.Result["state"] = STATE_UP
		}

		job.Result["total_ms"] = timeIt(test_start)
		testch <- job
		return
	}
}
