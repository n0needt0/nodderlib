usage:

standard Job json:
{
	"chkid": "string  //unique check id"
	"chktype": //one of legacy support "HTTP_GET", "HTTP_HEAD", "HTTP_POST", //generic "HTTP"
	"retrace":"bool   //whether or not use traceroute on fail"
	"config": {
		see below
	}
}

config parameters:

"useragent":"Useragent string"
"checkcerts":true|false //whether or not go strict on ssl
"request": "get|head|post|put|delete|patch" //note head has no content out

"uri": [schema]://user:password@host:port/query
"cookies":["fake1=faketest1", "fake2=faketest2"]
"headers":["Content-Fake:faketest1", "Content-Fake:faketest2"]
"postdata":["field1=test1", "field2=test2"]

"ok_codes": ["200","206","302"]
"ok_strings": ["fakestring1","fakestring2"] //strings searched in body
"ok_headers": ["fakeheader1", "fakeheade2"] //strings searched in header
"ok_json_values":["path.path...key=value.(number)|.(bool)|.(string)"] //without cast operator .(type) compares values as strings by default
"ok_json_nodes":["path.path...key"] //ensures node exists

"options":["-1","-4"....]
//valid options are

-1, --tlsv1

(SSL) Forces curl to use TLS version 1.x when negotiating with a remote TLS server. You can use options --tlsv1.0, --tlsv1.1, and --tlsv1.2 to control the TLS version more precisely (if the SSL backend in use supports such a level of control).

-2, --sslv2

(SSL) Forces curl to use SSL version 2 when negotiating with a remote SSL server. Sometimes curl is built without SSLv2 support. SSLv2 is widely considered insecure (see RFC 6176).

-3, --sslv3

(SSL) Forces curl to use SSL version 3 when negotiating with a remote SSL server. Sometimes curl is built without SSLv3 support. SSLv3 is widely considered insecure (see RFC 7568).

-4, --ipv4

This option tells curl to resolve names to IPv4 addresses only, and not for example try IPv6.

-6, --ipv6

This option tells curl to resolve names to IPv6 addresses only, and not for example try IPv4.

using as REST test

1. set proper request GET, PUT, DELETE, POST, PATCH, HEAD
2. set proper request header as required by API
		Content-Type: application/json
		or
		Content-Type: application/x-www-form-urlencoded
3. set proper body if using JSON in request as so
4. 	"postdata":["{JSON OBJECT GOES HERE}"] //no form element, requires application/json header

Comparing output JSON values
Works only for JSON in a body replies
1. specify what you expect in json reply
	{
		"expected_strings":[{"key1":"value1"}, {"key2":"value2"}],
		"expected_numbers":[{"key1":"value1"}, {"key2":"value2"}],
		"expected_bools":[{"key1":"value1"}, {"key2":"value2"}]
	}