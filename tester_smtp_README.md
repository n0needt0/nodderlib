usage:

standard Job json:
{
	"chkid": "string  //unique check id",
	"chktype":"SMTP",
	"retrace":"bool   //whether or not use traceroute on fail",
	"config: {
	"uri":"smtp(s)://user:password@host:port (omit for standard ports 25/465)",
	"skipverify":"bool //whether or not skip verify TLS certificates, default to true",
	"timeout":"int //timeout sec"
	}
}

