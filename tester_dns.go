/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package nodderlib

import (
	"bitbucket.org/n0needt0/keyr"
	"fmt"
	"github.com/miekg/dns"
	"net"
	"strings"
)

/*This method implements testing function that will be run in tester_base*/
func f_dns() Test {
	return func(job Job, testch chan Job) {
		defer func() { //recover in case channel closed
			if x := recover(); x != nil {
				log.Debug(fmt.Sprintf("f_dns failed on: %+v", x))
			}
		}()

		valid_records := []string{"DNS_A", "DNS_CNAME", "DNS_HINFO", "DNS_MX", "DNS_NS", "DNS_PTR", "DNS_SOA", "DNS_TXT", "DNS_AAAA", "DNS_SRV", "DNS_NAPTR", "DNS_A6", "DNS_ALL", "DNS_ANY"}
		k := keyr.New(job.Config)

		//validate host
		host, err := k.GetKeyAsString("host")
		if err != nil {
			job.Result["msg"] = err.Error()
			testch <- job
			return
		}

		//validate record type
		record_type, err := k.GetKeyAsString("record_type")
		if err != nil {
			record_type = "DNS_ANY" //DEFAULT_DNS_RECORD_TYPE
			job = job.setcfgdefault("record_type", record_type)
		}

		record_type = strings.ToUpper(record_type)

		if !k.StringsContainString(valid_records, record_type) {
			record_type = "DNS_ANY" //DEFAULT_DNS_RECORD_TYPE
			job = job.setcfgdefault("record_type", record_type)
		}

		job = job.setcfgvalue("record_type", record_type)

		//good to process

		dnscfg, _ := dns.ClientConfigFromFile("/etc/resolv.conf")

		c := new(dns.Client)

		m := new(dns.Msg)

		q := dns.TypeANY

		switch record_type {
		case "DNS_A":
			q = dns.TypeA
		case "DNS_CNAME":
			q = dns.TypeCNAME
		case "DNS_HINFO":
			q = dns.TypeHINFO
		case "DNS_MX":
			q = dns.TypeMX
		case "DNS_NS":
			q = dns.TypeNS
		case "DNS_PTR":
			q = dns.TypePTR
		case "DNS_SOA":
			q = dns.TypeSOA
		case "DNS_TXT":
			q = dns.TypeTXT
		case "DNS_AAAA":
			q = dns.TypeAAAA
		case "DNS_SRV":
			q = dns.TypeSRV
		case "DNS_NAPTR":
			q = dns.TypeSRV
		case "DNS_A6": //NOT used anymore
			q = dns.TypeAAAA
		}

		m.SetQuestion(dns.Fqdn(host), q)

		m.RecursionDesired = true

		test_start := makeTimestamp()

		r, _, err := c.Exchange(m, net.JoinHostPort(dnscfg.Servers[0], dnscfg.Port))

		job.Result["total_ms"] = timeIt(test_start)

		//failure to get dns obj
		if r == nil {
			job.Result["msg"] = fmt.Sprintf("DNS error: %s", err.Error())
			testch <- job
			return
		}

		if r.Rcode != dns.RcodeSuccess {
			job.Result["msg"] = fmt.Sprintf("DNS error %s %s invalid answer ", host, record_type)
			testch <- job
			return
		}

		cnt := 0
		dnsrec := ""
		for _, a := range r.Answer {
			cnt++
			dnsrec = dnsrec + a.String() + "\n"
		}

		if cnt > 0 {
			job.Result["state"] = STATE_UP
			testch <- job
			return
		}

		//if we here obviuously nothing returned :()
		job.Result["msg"] = fmt.Sprintf("DNS error %s %s invalid answer ", host, record_type)
		testch <- job
		return
	}
}
